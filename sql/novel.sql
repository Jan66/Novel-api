/*
 Navicat Premium Data Transfer

 Source Server         : novel
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : 106.15.235.206:3306
 Source Schema         : novel

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 04/03/2020 17:34:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 112 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', 'xx科技', 0, 'admin', '15888888888', 'test@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, 'admin', '15888888888', 'test@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, 'admin', '15888888888', 'test@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, 'admin', '15888888888', 'test@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-27 08:56:10');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, 'admin', '15888888888', 'test@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, 'admin', '15888888888', 'test1@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-05-15 07:46:27');
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, 'admin', '15888888888', 'test@qq.com', '0', '2', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, 'admin', '15888888888', 'test@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-05-20 09:53:41');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, 'admin', '15888888888', 'test@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, 'admin', '15888888888', 'test@qq.com', '0', '2', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 129 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '权限标识',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认跳转地址',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1049 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '/system', 'M', '0', '', 'Layout', '/system/index', 'fa fa-gears', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-27 09:44:51', '系统管理目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 2, '/system/user', 'C', '0', 'system:user:list', 'User', NULL, '', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 3, '/system/role', 'C', '0', 'system:role:list', 'Role', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 4, '/system/menu', 'C', '0', 'system:menu:list', 'Menu', '', '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-05-14 09:57:10', '');
INSERT INTO `sys_menu` VALUES (104, '操作日志', 1, 7, '/system/logs', 'C', '0', 'system:logs:list', 'Logs', NULL, '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-06-11 08:30:43', '操作日志');
INSERT INTO `sys_menu` VALUES (105, '部门管理', 1, 6, '/system/dept', 'C', '0', 'system:dept:list', 'Dept', NULL, '', 'admin', '2019-05-15 03:46:35', 'admin', '2019-06-11 08:30:30', '部门管理');
INSERT INTO `sys_menu` VALUES (106, '岗位管理', 1, 5, '/system/post', 'C', '0', 'system:post:list', 'Post', NULL, '#', '', '2019-06-11 08:29:53', 'admin', '2019-06-11 08:30:17', '岗位管理');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', 'F', '', 'system:user:query', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:10', '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', 'F', '', 'system:user:add', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', 'F', '', 'system:user:edit', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', 'F', '', 'system:user:remove', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '角色查询', 101, 1, '#', 'F', '', 'system:role:query', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:21', '');
INSERT INTO `sys_menu` VALUES (1005, '角色新增', 101, 2, '#', 'F', '', 'system:role:add', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '角色修改', 101, 3, '#', 'F', '', 'system:role:edit', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色删除', 101, 4, '#', 'F', '', 'system:role:remove', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '菜单查询', 102, 1, '#', 'F', '', 'system:menu:query', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:28', '');
INSERT INTO `sys_menu` VALUES (1009, '菜单新增', 102, 2, '#', 'F', '', 'system:menu:add', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '菜单修改', 102, 3, '#', 'F', '', 'system:menu:edit', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '菜单删除', 102, 4, '#', 'F', '', 'system:menu:remove', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '部门查询', 105, 1, '#', 'F', '', 'system:dept:query', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:44', '');
INSERT INTO `sys_menu` VALUES (1014, '部门新增', 105, 2, '#', 'F', '', 'system:dept:add', '', '', '', 'admin', '2019-05-15 03:51:29', 'admin', NULL, '部门新增');
INSERT INTO `sys_menu` VALUES (1015, '部门修改', 105, 3, '#', 'F', '', 'system:dept:edit', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门删除', 105, 4, '#', 'F', '', 'system:dept:remove', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '日志查询', 104, 1, '#', 'F', '', 'system:logs:query', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:51', '');
INSERT INTO `sys_menu` VALUES (1019, '日志删除', 104, 2, '#', 'F', '', 'system:logs:remove', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '日志清空', 104, 3, '#', 'F', '', 'system:logs:clean', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '日志导出', 104, 4, '#', 'F', '', 'system:logs:export', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '日志详情', 104, 5, '#', 'F', '', 'system:logs:detail', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '系统监控', 0, 2, '/monitor', 'M', '0', '', 'Layout', '/server', 'el-icon-camera', '', '2019-05-22 02:07:47', '', NULL, '系统监控');
INSERT INTO `sys_menu` VALUES (1024, '服务监控', 1023, 1, '/monitor/server', 'C', '0', 'monitor:server:list', 'Server', NULL, '#', '', '2019-05-22 02:09:16', '', NULL, '服务监控');
INSERT INTO `sys_menu` VALUES (1025, '岗位查询', 106, 1, '#', 'F', '', 'system:post:query', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:38', '');
INSERT INTO `sys_menu` VALUES (1026, '岗位新增', 106, 2, '#', 'F', '', 'system:post:add', '', '', '', 'admin', '2019-05-15 03:51:29', 'admin', NULL, '部门新增');
INSERT INTO `sys_menu` VALUES (1027, '岗位修改', 106, 3, '#', 'F', '', 'system:post:edit', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '岗位删除', 106, 4, '#', 'F', '', 'system:post:remove', '', '', '', 'admin', '2018-03-16 11:33:00', 'admin', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '在线用户', 1023, 2, '/monitor/online', 'C', '0', 'monitor:online:list', 'Online', NULL, '#', '', '2019-12-12 07:45:25', 'test', '2019-12-12 10:01:27', '');
INSERT INTO `sys_menu` VALUES (1031, '强退', 1030, 1, '#', 'F', '', 'monitor:online:forceLogout', NULL, NULL, '#', 'test', '2019-12-17 03:28:53', 'admin', '2019-12-18 06:20:17', '强制退出登录用户');
INSERT INTO `sys_menu` VALUES (1032, '数据监控', 1023, 3, '/monitor/druid', 'C', '0', 'monitor:druid:list', 'Druid', NULL, '#', 'admin', '2019-12-17 07:53:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '登录日志', 1, 8, '/system/logininfor', 'C', '0', 'system:logininfor:list', 'Logininfor', NULL, '#', 'admin', '2019-12-20 07:55:39', 'admin', '2019-12-20 07:55:52', '');
INSERT INTO `sys_menu` VALUES (1034, '登录查询', 1033, 1, '#', 'F', '', 'system:logininfor:query', NULL, NULL, '#', 'admin', '2019-12-20 07:56:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '登录删除', 1033, 2, '#', 'F', '', 'system:logininfor:remove', NULL, NULL, '#', 'admin', '2019-12-20 07:57:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '登录清空', 1033, 3, '#', 'F', '', 'system:logininfor:clean', NULL, NULL, '#', 'admin', '2019-12-20 07:58:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '登录导出', 1033, 4, '#', 'F', '', 'system:logininfor:export', NULL, NULL, '#', 'admin', '2019-12-20 07:59:47', 'admin', '2020-03-04 16:17:10', '');
INSERT INTO `sys_menu` VALUES (1038, '定时任务', 1023, 4, '/monitor/job', 'C', '0', 'monitor:job:list', 'Job', NULL, '#', 'admin', '2020-03-02 17:30:43', 'admin', '2020-03-03 11:41:46', '定时任务');
INSERT INTO `sys_menu` VALUES (1039, '任务新增', 1038, 1, '#', 'F', '', 'monitor:job:add', NULL, NULL, '#', 'admin', '2020-03-02 17:36:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '任务修改', 1038, 2, '#', 'F', '', 'monitor:job:edit', NULL, NULL, '#', 'admin', '2020-03-02 17:37:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '任务删除', 1038, 3, '#', 'F', '', 'monitor:job:remove', NULL, NULL, '#', 'admin', '2020-03-02 17:37:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '状态修改', 1038, 4, '#', 'F', '', 'monitor:job:changeStatus', NULL, NULL, '#', 'admin', '2020-03-02 17:38:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '任务查询', 1038, 5, '#', 'F', '', 'monitor:job:query', NULL, NULL, '#', 'admin', '2020-03-02 17:39:47', 'admin', '2020-03-03 09:53:47', '');
INSERT INTO `sys_menu` VALUES (1044, '任务详情', 1038, 6, '#', 'F', '', 'monitor:job:detail', NULL, NULL, '#', 'admin', '2020-03-03 11:24:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '任务日志', 1038, 7, '#', 'F', '', 'monitor:job:log', NULL, NULL, '#', 'admin', '2020-03-03 14:49:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '岗位导出', 106, 5, '#', 'F', '', 'system:post:export', NULL, NULL, '#', 'admin', '2020-03-04 16:18:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '角色导出', 101, 5, '#', 'F', '', 'system:role:export', NULL, NULL, '#', 'admin', '2020-03-04 16:19:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '用户导出', 100, 5, '#', 'F', '', 'system:user:export', NULL, NULL, '#', 'admin', '2020-03-04 16:20:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '系统工具', 0, 3, '/tool', 'M', '0', '', 'Layout', NULL, 'fa fa-wrench', 'admin', '2020-03-25 17:18:46', '', NULL, '系统工具');
INSERT INTO `sys_menu` VALUES (1051, '代码生成', 1050, 1, '/tool/gen', 'C', '0', 'tool:gen:list', 'Gen', NULL, '#', 'admin', '2020-03-25 17:22:06', '', NULL, '代码生成');
INSERT INTO `sys_menu` VALUES (1069, '生成代码', 1051, 1, '#', 'F', '', 'tool:gen:code', NULL, NULL, '#', 'admin', '2020-03-26 14:08:47', '', NULL, '生成代码');
INSERT INTO `sys_menu` VALUES (1070, '生成查询', 1051, 2, '#', 'F', '', 'tool:gen:query', NULL, NULL, '#', 'admin', '2020-03-26 14:11:10', '', NULL, '生成查询');
INSERT INTO `sys_menu` VALUES (1071, '参数管理', 1, 6, '/system/config', 'C', '0', 'system:config:list', 'Config', NULL, '', 'admin', '2020-03-25 17:22:06', 'admin', '2020-07-08 11:52:20', '参数配置菜单');
INSERT INTO `sys_menu` VALUES (1072, '参数查询', 1071, 1, '#', 'F', '', 'system:config:query', NULL, NULL, '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:10', '参数配置查询');
INSERT INTO `sys_menu` VALUES (1073, '参数新增', 1071, 2, '#', 'F', '', 'system:config:add', NULL, NULL, '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:10', '参数配置新增');
INSERT INTO `sys_menu` VALUES (1074, '参数修改', 1071, 2, '#', 'F', '', 'system:config:edit', NULL, NULL, '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:10', '参数配置修改');
INSERT INTO `sys_menu` VALUES (1075, '参数删除', 1071, 2, '#', 'F', '', 'system:config:remove', NULL, NULL, '', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-20 07:09:10', '参数配置删除');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `processing_time` int(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '处理耗时',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 548 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-29 07:03:16', '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-29 07:01:05', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '\0' COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', 1, '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-04 17:26:16', '管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-04 17:26:55', '普通角色');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 100);
INSERT INTO `sys_role_menu` VALUES (1, 101);
INSERT INTO `sys_role_menu` VALUES (1, 102);
INSERT INTO `sys_role_menu` VALUES (1, 104);
INSERT INTO `sys_role_menu` VALUES (1, 105);
INSERT INTO `sys_role_menu` VALUES (1, 106);
INSERT INTO `sys_role_menu` VALUES (1, 1000);
INSERT INTO `sys_role_menu` VALUES (1, 1001);
INSERT INTO `sys_role_menu` VALUES (1, 1002);
INSERT INTO `sys_role_menu` VALUES (1, 1003);
INSERT INTO `sys_role_menu` VALUES (1, 1004);
INSERT INTO `sys_role_menu` VALUES (1, 1005);
INSERT INTO `sys_role_menu` VALUES (1, 1006);
INSERT INTO `sys_role_menu` VALUES (1, 1007);
INSERT INTO `sys_role_menu` VALUES (1, 1008);
INSERT INTO `sys_role_menu` VALUES (1, 1009);
INSERT INTO `sys_role_menu` VALUES (1, 1010);
INSERT INTO `sys_role_menu` VALUES (1, 1011);
INSERT INTO `sys_role_menu` VALUES (1, 1012);
INSERT INTO `sys_role_menu` VALUES (1, 1014);
INSERT INTO `sys_role_menu` VALUES (1, 1015);
INSERT INTO `sys_role_menu` VALUES (1, 1016);
INSERT INTO `sys_role_menu` VALUES (1, 1018);
INSERT INTO `sys_role_menu` VALUES (1, 1019);
INSERT INTO `sys_role_menu` VALUES (1, 1020);
INSERT INTO `sys_role_menu` VALUES (1, 1021);
INSERT INTO `sys_role_menu` VALUES (1, 1022);
INSERT INTO `sys_role_menu` VALUES (1, 1023);
INSERT INTO `sys_role_menu` VALUES (1, 1024);
INSERT INTO `sys_role_menu` VALUES (1, 1025);
INSERT INTO `sys_role_menu` VALUES (1, 1026);
INSERT INTO `sys_role_menu` VALUES (1, 1027);
INSERT INTO `sys_role_menu` VALUES (1, 1028);
INSERT INTO `sys_role_menu` VALUES (1, 1030);
INSERT INTO `sys_role_menu` VALUES (1, 1031);
INSERT INTO `sys_role_menu` VALUES (1, 1032);
INSERT INTO `sys_role_menu` VALUES (1, 1033);
INSERT INTO `sys_role_menu` VALUES (1, 1034);
INSERT INTO `sys_role_menu` VALUES (1, 1035);
INSERT INTO `sys_role_menu` VALUES (1, 1036);
INSERT INTO `sys_role_menu` VALUES (1, 1037);
INSERT INTO `sys_role_menu` VALUES (1, 1038);
INSERT INTO `sys_role_menu` VALUES (1, 1039);
INSERT INTO `sys_role_menu` VALUES (1, 1040);
INSERT INTO `sys_role_menu` VALUES (1, 1041);
INSERT INTO `sys_role_menu` VALUES (1, 1042);
INSERT INTO `sys_role_menu` VALUES (1, 1043);
INSERT INTO `sys_role_menu` VALUES (1, 1044);
INSERT INTO `sys_role_menu` VALUES (1, 1045);
INSERT INTO `sys_role_menu` VALUES (1, 1046);
INSERT INTO `sys_role_menu` VALUES (1, 1047);
INSERT INTO `sys_role_menu` VALUES (1, 1048);
INSERT INTO `sys_role_menu` VALUES (1, 1050);
INSERT INTO `sys_role_menu` VALUES (1, 1051);
INSERT INTO `sys_role_menu` VALUES (1, 1069);
INSERT INTO `sys_role_menu` VALUES (1, 1070);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像路径',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phone_number` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `age` int(3) NULL DEFAULT NULL COMMENT '年龄',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_nameUNIQUE`(`user_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 100, 'admin', '2b25ad8ab588450aabbd4a0e5ac7b3d5', 'admin', '/resources/file/2020/01/15/0d217877fbcb8ab3ff94af02db1fbc8f.png', '0', '1102@163.com', '15888888889', 18, '31c4ca', '0', '0', '127.0.0.1', '2020-03-04 17:29:34', 'admin', '2019-04-28 17:18:07', 'admin', '2020-03-04 17:29:32', NULL);
INSERT INTO `sys_user` VALUES (2, 105, 'test', 'e6cfb51d694e9a03a1c26e99f3e80635', '张三', '/resources/file/2020/03/04/2be1df7818d10990d86d6e57b022b479.jpeg', '0', 'test1@test.com', '18888888888', 18, 'b3daee', '0', '0', '127.0.0.1', '2020-03-04 17:27:35', '', '2019-11-26 09:52:32', 'admin', '2020-03-04 17:29:22', NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `post_id` int(11) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (2, 4);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '参数键值',
  `config_type` bit(1) NOT NULL DEFAULT b'0' COMMENT '系统内置（1是 0否）',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', b'1', '初始化密码 123456', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_config` VALUES (2, '系统功能-网盘', 'sys.index.disk', 'true', b'1', '是否启用个人网盘功能', 'admin', '2020-07-27 13:32:57', '', NULL);

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
 `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
 `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
 `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
 `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
 `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
 `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
 `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
 `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
 `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
 `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
 `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
 `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
 `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
 `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
 `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
 `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
 `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
 `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
 `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 158 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
