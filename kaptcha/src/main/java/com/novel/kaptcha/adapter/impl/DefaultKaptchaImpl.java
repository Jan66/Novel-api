package com.novel.kaptcha.adapter.impl;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.novel.kaptcha.adapter.AbstractKaptchaImpl;
import com.novel.kaptcha.cache.KaptchaCache;
import com.novel.kaptcha.config.KaptchaProperties;
import com.novel.kaptcha.exception.KaptchaIsEmptyException;
import com.novel.kaptcha.exception.KaptchaRenderException;
import com.novel.kaptcha.exception.KaptchaTimeoutException;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.awt.image.BufferedImage;

/**
 * 默认随机验证码实现类
 *
 * @author novel
 * @date 2019/12/2
 */
public class DefaultKaptchaImpl extends AbstractKaptchaImpl {

    public DefaultKaptchaImpl(DefaultKaptcha kaptcha, KaptchaProperties kaptchaProperties) {
        super(kaptcha, kaptchaProperties);
    }

    @Override
    public BufferedImage render(@NotNull String key) {
        try {
            String capText = kaptcha.createText();
            KaptchaCache cache = kaptchaCacheManager.getCache((KaptchaProperties.PREFIX + "_" + this.kaptchaProperties.getType()).toUpperCase());
            cache.saveKaptcha(key, capText);
            return kaptcha.createImage(capText);
        } catch (Exception e) {
            throw new KaptchaRenderException();
        }
    }

    @Override
    public boolean validate(@NotNull String key, String code) {
        if (!StringUtils.hasLength(code) || !StringUtils.hasLength(code.trim())) {
            //需要验证的验证码不存在，用户未输入
            throw new KaptchaIsEmptyException();
        }
        String kaptchaCode = getKaptchaCode(key);

        if (kaptchaProperties.isCaseSensitivity()) {
            //区分大小写
            return kaptchaCode.equals(code);
        } else {
            //不区分大小写
            return kaptchaCode.equalsIgnoreCase(code);
        }
    }

    @Override
    public String getKaptchaCode(@NotNull String key) {
        KaptchaCache cache = kaptchaCacheManager.getCache((KaptchaProperties.PREFIX + "_" + this.kaptchaProperties.getType()).toUpperCase());
        String kaptcha = cache.getKaptcha(key);
        cache.removeKaptcha(key);
        if (!StringUtils.hasLength(kaptcha)) {
            //验证码不存在，已经过期
            throw new KaptchaTimeoutException();
        }
        return kaptcha;
    }
}
