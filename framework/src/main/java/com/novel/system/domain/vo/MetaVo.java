package com.novel.system.domain.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 路由附带内容
 *
 * @author novel
 * @date 2019/4/11
 */
@Data
@ToString
public class MetaVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 路由标题
     */
    private String title;
    /**
     * 路由图标
     */
    private String icon;
}
