package com.novel.system.service;

import com.novel.system.domain.SysConfig;

import java.util.List;

/**
 * 参数配置 服务层
 *
 * @author novel
 * @date 2020/07/08
 */
public interface SysConfigService {

    /**
     * 查询参数配置信息
     *
     * @param id 参数配置ID
     * @return 参数配置信息
     */
    SysConfig selectConfigById(Long id);

    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    List<SysConfig> selectConfigList(SysConfig config);

    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    boolean insertConfig(SysConfig config);

    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    boolean updateConfig(SysConfig config);

    /**
     * 保存参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    boolean saveConfig(SysConfig config);

    /**
     * 删除参数配置信息
     *
     * @param id 参数配置ID
     * @return 结果
     */
    boolean deleteConfigById(Long id);

    /**
     * 批量删除参数配置信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    boolean batchDeleteConfig(Long[] ids);

    /**
     * 校验参数key是否唯一
     *
     * @param config 参数信息
     * @return 结果
     */
    String checkConfigKeyUnique(SysConfig config);


    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    String selectConfigByKey(String configKey);
}
