package com.novel.service.impl;


import com.novel.common.utils.support.Convert;
import com.novel.domain.GenTableColumn;
import com.novel.mapper.GenTableColumnMapper;
import com.novel.service.IGenTableColumnService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 表 字段信息 服务层实现
 *
 * @author novel
 * @date 2021/5/7 19:41
 */
@Service
public class GenTableColumnServiceImpl implements IGenTableColumnService {
    private final GenTableColumnMapper genTableColumnMapper;

    public GenTableColumnServiceImpl(GenTableColumnMapper genTableColumnMapper) {
        this.genTableColumnMapper = genTableColumnMapper;
    }

    /**
     * 查询业务字段列表
     *
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
    @Override
    public List<GenTableColumn> selectGenTableColumnListByTableId(Long tableId) {
        return genTableColumnMapper.selectGenTableColumnListByTableId(tableId);
    }

    /**
     * 新增业务字段
     *
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    @Override
    public int insertGenTableColumn(GenTableColumn genTableColumn) {
        return genTableColumnMapper.insertGenTableColumn(genTableColumn);
    }

    /**
     * 修改业务字段
     *
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    @Override
    public int updateGenTableColumn(GenTableColumn genTableColumn) {
        return genTableColumnMapper.updateGenTableColumn(genTableColumn);
    }

    /**
     * 删除业务字段对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGenTableColumnByIds(String ids) {
        return genTableColumnMapper.deleteGenTableColumnByIds(Convert.toLongArray(ids));
    }
}
