package com.novel.common.exception.file;

/**
 * 非法文件异常
 *
 * @author novel
 * @date 2019/12/27
 */
public class IllegalFileException extends FileException {
    public IllegalFileException(String code, Object[] args) {
        super(code, args);
    }

    public IllegalFileException(String module, String code, Object[] args, String defaultMessage) {
        super(module, code, args, defaultMessage);
    }

    public IllegalFileException(String module, String code, Object[] args) {
        super(module, code, args);
    }

    public IllegalFileException(String module, String defaultMessage) {
        super(module, defaultMessage);
    }

    public IllegalFileException(String defaultMessage) {
        super(defaultMessage);
    }
}
