package com.novel.common.exception.file;

/**
 * 文件名称超长限制异常类
 *
 * @author novel
 * @date 2019/5/24
 */
public class FileNameLengthLimitExceededException extends FileException {
    private static final long serialVersionUID = 1L;

    public FileNameLengthLimitExceededException(int defaultFileNameLength) {
        super("upload.filename.exceed.length", new Object[]{defaultFileNameLength});
    }
}
